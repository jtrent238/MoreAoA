package com.jtrent238.moreaoa.items.bowls;

import net.minecraft.item.Item;
import net.tslat.aoa3.item.tool.misc.InfusionBowl;

public class WoodBowl extends InfusionBowl {

	public WoodBowl(String name, String registryName, int durability) {
		super("WoodBowl", "wood_bowl", 100);
	}

}
